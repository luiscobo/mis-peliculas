package com.uah.mispeliculas;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.uah.mispeliculas.baseDatos.BaseDatos;
import com.uah.mispeliculas.model.Pelicula;
import com.uah.mispeliculas.util.DatePickerFragment;

import java.util.ArrayList;
import java.util.Objects;

public class EditarPeliculaActivity extends AppCompatActivity {

    private int idPelicula;
    private BaseDatos baseDatos;
    private SQLiteDatabase db;
    private TextInputLayout tilFechaVisionado;
    private TextInputLayout tilCiudad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pelicula);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        baseDatos = new BaseDatos(this);

        idPelicula =Integer.parseInt(getIntent().getStringExtra("ID_KEY"));
        Pelicula peliculModificar = getPeliculaById(idPelicula);

        ImageView ivImgServer = (ImageView) findViewById(R.id.ivImgServer);
        TextView tvTituloPelicula = (TextView) findViewById(R.id.tvTituloServer);
        TextView tvGenero = (TextView) findViewById(R.id.tvGeneroServer);
        TextView tvAnnio = (TextView) findViewById(R.id.tvAnnioServer);
        TextView tvActores = (TextView) findViewById(R.id.tvActoresServer);
        TextView tvPais = (TextView) findViewById(R.id.tvPaisServer);

        Glide.with(this).load(peliculModificar.getUrlImg()).into(ivImgServer);

        tvTituloPelicula.setText(getString(R.string.label_titulo) + ": "+ peliculModificar.getTitulo());
        tvGenero.setText(getString(R.string.label_genero)+ ": "+ peliculModificar.getGenero());
        tvAnnio.setText(getString(R.string.label_annio)+ ": "+ peliculModificar.getAnnio());
        tvActores.setText(getString(R.string.label_actores)+ ": "+ peliculModificar.getActores());
        tvPais.setText(getString(R.string.label_pais)+ ": "+ peliculModificar.getPais());

        tilFechaVisionado = (TextInputLayout) findViewById(R.id.tilFechaVisionado);
        tilFechaVisionado.getEditText().setText(peliculModificar.getFechaVisionado());
        tilFechaVisionado.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        tilCiudad = (TextInputLayout) findViewById(R.id.tilCiudad);
        tilCiudad.getEditText().setText(peliculModificar.getCiudad());

        Button btnGuardar = findViewById(R.id.btnGuardar);
        // Listener para el boton guardar pelicula
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validamos los datos
                if(validate()) {
                    //Obtenemos el modo escritura en la bd
                    db = baseDatos.getWritableDatabase();
                    //comprobamos que se ha abierto correctemente
                    if (db != null)  {
                        peliculModificar.setCiudad(tilCiudad.getEditText().getText().toString());
                        peliculModificar.setFechaVisionado(tilFechaVisionado.getEditText().getText().toString());
                        baseDatos.updatePelicula(db, peliculModificar);
                        //Cerramos la base de datos
                        db.close();
                        Toast.makeText(getApplicationContext(), getString(R.string.mensaje_modificar_ok), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.mensaje_modificar_ko), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
    // Obtenemos la calificacion a modificar
    private Pelicula getPeliculaById(int idPelicula) {

        db = baseDatos.getReadableDatabase();
        Cursor c = baseDatos.getPeliculaById(db,idPelicula);
        ArrayList<Pelicula> listaPeliculas = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                listaPeliculas.add(new Pelicula(
                    c.getString(0),
                    c.getString(1),
                    c.getString(2),
                    c.getString(3),
                    c.getString(4),
                    c.getString(5),
                    c.getString(6),
                    c.getString(7),
                    c.getString(8)
                ));
            } while(c.moveToNext());
        }
        db.close();
        return listaPeliculas.get(0);
    }

    /* Función para validar los datos del formulario */
    private boolean validate() {
        boolean valid = true;

        if (tilCiudad.getEditText().getText().toString().equals("")) {
            tilCiudad.getEditText().setError(getString(R.string.mensaje_campo_obligatorio));
            valid = false;
        } else {
            tilCiudad.setError(null);
        }
        if (tilFechaVisionado.getEditText().getText().toString().equals("")) {
            tilFechaVisionado.getEditText().setError(getString(R.string.mensaje_campo_obligatorio));
            valid = false;
        } else {
            tilFechaVisionado.setError(null);
        }

        return valid;
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 porque enero es 0
                final String selectedDate = day + "/" + (month+1) + "/" + year;
                Objects.requireNonNull(tilFechaVisionado.getEditText()).setText(selectedDate);
            }
        });
        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editar_pelicula, menu);
        int positionOfMenuItem = 0;
        MenuItem item = menu.getItem(positionOfMenuItem);
        SpannableString s = new SpannableString(getString(R.string.dialog_eliminar_titulo));
        s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, s.length(), 0);
        item.setTitle(s);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //Preguntamos al usuario si realmente desea salir de la aplicación
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(EditarPeliculaActivity.this);
            dialogo1.setTitle(getString(R.string.dialog_eliminar_titulo));
            dialogo1.setMessage(getString(R.string.dialog_eliminar_mensaje));
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton(getString(R.string.dialog_eliminar_aceptar), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    db = baseDatos.getReadableDatabase();
                    baseDatos.deletePelicula(db,idPelicula);
                    db.close();
                    Toast.makeText(getApplicationContext(), getString(R.string.mensaje_eliminar_ok), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            dialogo1.setNegativeButton(getString(R.string.dialog_eliminar_cancelar), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                }
            });
            dialogo1.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}