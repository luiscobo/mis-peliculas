package com.uah.mispeliculas;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.uah.mispeliculas.adaptador.AdaptadorPeliculas;
import com.uah.mispeliculas.baseDatos.BaseDatos;
import com.uah.mispeliculas.model.Pelicula;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private BaseDatos baseDatos;
    private SQLiteDatabase db;

    private RecyclerView rvPeliculas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        baseDatos = new BaseDatos(this);
        rvPeliculas = findViewById(R.id.rvPeliculas);
        rvPeliculas.setHasFixedSize(true);

        // Nuestro RecyclerView usará un linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvPeliculas.setLayoutManager(layoutManager);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NuevaPeliculaActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        obtenerPeliculas();
        super.onStart();
    }

    public void obtenerPeliculas() {
        db = baseDatos.getReadableDatabase();
        Cursor c = baseDatos.getAllPeliculas(db);
        ArrayList<Pelicula> listaCalificaciones = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                listaCalificaciones.add(new Pelicula(
                        c.getString(0),
                        c.getString(1),
                        c.getString(2),
                        c.getString(3),
                        c.getString(4),
                        c.getString(5),
                        c.getString(6),
                        c.getString(7),
                        c.getString(8)
                ));
            } while(c.moveToNext());
        }
        db.close();

        // Asociamos un adapter
        AdaptadorPeliculas adaptadorCalificaciones = new AdaptadorPeliculas(listaCalificaciones);
        adaptadorCalificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // para el evento onClick lanzamos la actividad de modificar calificacion y pasamos el Id por parametro.
                String id = listaCalificaciones.get(rvPeliculas.getChildAdapterPosition(v)).getId();
                Intent intent = new Intent(getApplicationContext(), EditarPeliculaActivity.class);
                intent.putExtra("ID_KEY", id);
                startActivity(intent);
            }
        });
        rvPeliculas.setAdapter(adaptadorCalificaciones);
    }

    @Override
    public void onBackPressed() {

        //Preguntamos al usuario si realmente desea salir de la aplicación
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(MainActivity.this);
        dialogo1.setTitle(getString(R.string.dialog_salir_titulo));
        dialogo1.setMessage(getString(R.string.dialog_salir_mensaje));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(getString(R.string.dialog_salir_aceptar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                finish();
            }
        });
        dialogo1.setNegativeButton(getString(R.string.dialog_salir_cancelar), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
            }
        });
        dialogo1.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);int positionOfMenuItem = 0;
        MenuItem item = menu.getItem(positionOfMenuItem);
        SpannableString s = new SpannableString(getString(R.string.dialog_eliminar_todas_titulo));
        s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, s.length(), 0);
        item.setTitle(s);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //Preguntamos al usuario si realmente desea salir de la aplicación
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(MainActivity.this);
            dialogo1.setTitle(getString(R.string.dialog_eliminar_todas_titulo));
            dialogo1.setMessage(getString(R.string.dialog_eliminar_todas_mensaje));
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton(getString(R.string.dialog_eliminar_todas_aceptar), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    db = baseDatos.getReadableDatabase();
                    baseDatos.deleteAllPeliculas(db);
                    db.close();
                    Toast.makeText(getApplicationContext(), getString(R.string.mensaje_eliminar_todas_ok), Toast.LENGTH_SHORT).show();
                    obtenerPeliculas();
                }
            });
            dialogo1.setNegativeButton(getString(R.string.dialog_eliminar_todas_cancelar), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                }
            });
            dialogo1.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}