package com.uah.mispeliculas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.uah.mispeliculas.baseDatos.BaseDatos;
import com.uah.mispeliculas.model.Pelicula;
import com.uah.mispeliculas.model.PeliculaServer;
import com.uah.mispeliculas.util.DatePickerFragment;
import com.uah.mispeliculas.util.VolleySingleton;
import org.json.JSONObject;


public class NuevaPeliculaActivity extends AppCompatActivity {

    private TextInputLayout tilTituloPelicula;
    private final String url = "https://www.omdbapi.com/?t=";
    private final String apiKey = "&apikey=d93c3d3d";
    private String tituloPelicula;
    private PeliculaServer peliculaServer;
    private Pelicula pelicula;
    private LinearLayout datosPelicula;
    private TextInputLayout tilFechaVisionado;
    private TextInputLayout tilCiudad;
    private Button btnGuardar;
    private BaseDatos baseDatos;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_pelicula);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        baseDatos = new BaseDatos(this);
        tilTituloPelicula = (TextInputLayout) findViewById(R.id.tilTituloPelicula);
        datosPelicula = (LinearLayout) findViewById(R.id.datosPelicula);
        datosPelicula.setVisibility(View.INVISIBLE);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validamos los datos
                if(validate()) {
                    pelicula = new Pelicula();
                    pelicula.setTitulo(peliculaServer.getTitle());
                    pelicula.setActores(peliculaServer.getActors());
                    pelicula.setUrlImg(peliculaServer.getPoster());
                    pelicula.setCiudad(tilCiudad.getEditText().getText().toString());
                    pelicula.setFechaVisionado(tilFechaVisionado.getEditText().getText().toString());
                    pelicula.setGenero(peliculaServer.getGenre());
                    pelicula.setAnnio(peliculaServer.getYear());
                    pelicula.setPais(peliculaServer.getCountry());
                    //Obtenemos el modo escritura en la bd
                    db = baseDatos.getWritableDatabase();
                    //comprobamos que se ha abierto correctemente
                    if (db != null)                {
                        baseDatos.insertPelicula(db, pelicula);
                        //Cerramos la base de datos
                        db.close();
                        Toast.makeText(getApplicationContext(), getString(R.string.mensaje_anadir_ok), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.mensaje_anadir_ko), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        tilFechaVisionado = (TextInputLayout) findViewById(R.id.tilFechaVisionado);
        tilFechaVisionado.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        tilCiudad = (TextInputLayout) findViewById(R.id.tilCiudad);
        ImageButton btnBuscar = findViewById(R.id.btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProgressDialog progressBarDialog = new ProgressDialog(NuevaPeliculaActivity.this);
                progressBarDialog.setMessage(getString(R.string.mensaje_progressBar));
                progressBarDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBarDialog.show();

                // Obtenemos el titulo de la pelicula a buscar
                tituloPelicula = tilTituloPelicula.getEditText().getText().toString();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.GET, url+tituloPelicula+apiKey, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            // Procesamos la respuesta de la Api
                            procesarRespuesta(response);
                            progressBarDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO: Handle error
                            progressBarDialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.mensaje_buscar_ko), Toast.LENGTH_SHORT).show();
                        }
                    }
                    );
                VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
            }
        });

    };

    // Procesomos la respuesta del servicio y visualizamos los datos principales de la  pelicula.
    private void procesarRespuesta(JSONObject response) {
        Gson gson = new Gson();

        // Converitmos el Json de respuesta al Objeto PeliculaServer
        peliculaServer = gson.fromJson(String.valueOf(response), PeliculaServer.class);
        if (peliculaServer.getResponse().equals("True")){

            btnGuardar.setVisibility(View.VISIBLE);
            datosPelicula.setVisibility(View.VISIBLE);

            ImageView ivImgServer = (ImageView) findViewById(R.id.ivImgServer);
            TextView tvTituloPelicula = (TextView) findViewById(R.id.tvTituloServer);
            TextView tvGenero = (TextView) findViewById(R.id.tvGeneroServer);
            TextView tvAnnio = (TextView) findViewById(R.id.tvAnnioServer);
            TextView tvActores = (TextView) findViewById(R.id.tvActoresServer);
            TextView tvPais = (TextView) findViewById(R.id.tvPaisServer);

            Glide.with(this).load(peliculaServer.getPoster()).into(ivImgServer);
            tvTituloPelicula.setText(getString(R.string.label_titulo) + ": "+ peliculaServer.getTitle());
            tvGenero.setText(getString(R.string.label_genero)+ ": "+ peliculaServer.getGenre());
            tvAnnio.setText(getString(R.string.label_annio)+ ": "+ peliculaServer.getYear());
            tvActores.setText(getString(R.string.label_actores)+ ": "+ peliculaServer.getActors());
            tvPais.setText(getString(R.string.label_pais)+ ": "+ peliculaServer.getCountry());

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.mensaje_buscar_no), Toast.LENGTH_SHORT).show();
        }

    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 porque enero es 0
                final String selectedDate = day + "/" + (month+1) + "/" + year;
                tilFechaVisionado.getEditText().setText(selectedDate);
            }
        });

        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    /* Función para validar los datos del formulario */
    private boolean validate() {
        boolean valid = true;

        if (tilCiudad.getEditText().getText().toString().equals("")) {
            tilCiudad.getEditText().setError(getString(R.string.mensaje_campo_obligatorio));
            valid = false;
        } else {
            tilCiudad.setError(null);
        }
        if (tilFechaVisionado.getEditText().getText().toString().equals("")) {
            tilFechaVisionado.getEditText().setError(getString(R.string.mensaje_campo_obligatorio));
            valid = false;
        } else {
            tilFechaVisionado.setError(null);
        }

        return valid;
    }
}