package com.uah.mispeliculas.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uah.mispeliculas.R;
import com.uah.mispeliculas.model.Pelicula;

import java.util.ArrayList;

public class AdaptadorPeliculas extends RecyclerView.Adapter<AdaptadorPeliculas.ViewHolder>  implements View.OnClickListener{

    private final ArrayList<Pelicula> peliculas;
    private View.OnClickListener listener;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView tvItemTitulo;
        public TextView tvItemCiudad;
        public TextView tvItemFechaVisionado;
        public TextView tvItemActores;
        public ImageView ivItemImg;

        public ViewHolder(View v) {
            super(v);
            tvItemTitulo = (TextView) v.findViewById(R.id.tvItemTitulo);
            tvItemCiudad = (TextView) v.findViewById(R.id.tvItemCiudad);
            tvItemFechaVisionado = (TextView) v.findViewById(R.id.tvItemFechaVisionado);
            tvItemActores = (TextView) v.findViewById(R.id.tvItemActores);
            ivItemImg = (ImageView) v.findViewById(R.id.ivItemImg);
        }
    }

    public AdaptadorPeliculas(ArrayList<Pelicula> lista) {
        this.peliculas =lista;
    }

    @Override
    public int getItemCount() {
        return peliculas.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_lista_pelicula, viewGroup, false);

        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Pelicula item = peliculas.get(i);
        viewHolder.tvItemTitulo.setText(item.getTitulo());
        viewHolder.tvItemCiudad.setText(item.getCiudad());
        viewHolder.tvItemFechaVisionado.setText(item.getFechaVisionado());
        viewHolder.tvItemActores.setText(item.getActores());
        Glide.with(context).load(item.getUrlImg()).into(viewHolder.ivItemImg);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

}