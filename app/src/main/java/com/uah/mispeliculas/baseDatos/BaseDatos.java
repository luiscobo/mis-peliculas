package com.uah.mispeliculas.baseDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.uah.mispeliculas.model.Pelicula;

public class BaseDatos extends SQLiteOpenHelper  {

    /**
     * Controlador de la bd
     */
    public final static String DATABASE_NAME = "Peliculas.db";
    public final static int DATABASE_VERSION = 2;

    String createSQL = "CREATE TABLE " + ConfigBaseDatos.Peliculas.TABLE_NAME + " (" +
            ConfigBaseDatos.Peliculas.COLUMN_ID + " INTEGER PRIMARY KEY," +
            ConfigBaseDatos.Peliculas.COLUMN_TITULO + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_ACTORES + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_URL_IMG + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_FECHA_VISIONADO + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_CIUDAD + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_GENERO + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_ANNIO + " TEXT," +
            ConfigBaseDatos.Peliculas.COLUMN_PAIS + " TEXT)";

    String dropSQL =  "DROP TABLE IF EXISTS " + ConfigBaseDatos.Peliculas.TABLE_NAME;

    public BaseDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //Creamos la tabla
        db.execSQL(createSQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Eliminamos la tabla y la volvemos a crear
        db.execSQL(dropSQL);
        onCreate(db);
    }

    // Guarda una nueva pelicula.
    public void insertPelicula(SQLiteDatabase db, Pelicula pelicula){
        // Preparamos el registro  a insertar
        ContentValues nuevaPelicula = new ContentValues();
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_TITULO, pelicula.getTitulo());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_ACTORES, pelicula.getActores());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_URL_IMG, pelicula.getUrlImg());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_FECHA_VISIONADO, pelicula.getFechaVisionado());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_CIUDAD, pelicula.getCiudad());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_GENERO, pelicula.getGenero());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_ANNIO, pelicula.getAnnio());
        nuevaPelicula.put(ConfigBaseDatos.Peliculas.COLUMN_PAIS, pelicula.getPais());

        // Realizamos el insert en la base de datos.
        db.insert(ConfigBaseDatos.Peliculas.TABLE_NAME, null, nuevaPelicula);
    }

    // Elimina un pelicula por su ID
    public void deletePelicula(SQLiteDatabase db, int id){
        String[] args = new String[] {Integer.toString(id)};
        db.delete(ConfigBaseDatos.Peliculas.TABLE_NAME, "id=?", args);
    }

    //Elimina todas las peliculas
    public void deleteAllPeliculas(SQLiteDatabase db){
        db.execSQL("delete from "+ ConfigBaseDatos.Peliculas.TABLE_NAME);
    }

    // Actualiza una nueva pelicula.
    public void updatePelicula(SQLiteDatabase db, Pelicula pelicula){
        // Preparamos el registro  a insertar
        ContentValues peliculaActualizar = new ContentValues();
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_TITULO, pelicula.getTitulo());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_ACTORES, pelicula.getActores());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_URL_IMG, pelicula.getUrlImg());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_FECHA_VISIONADO, pelicula.getFechaVisionado());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_CIUDAD, pelicula.getCiudad());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_GENERO, pelicula.getGenero());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_ANNIO, pelicula.getAnnio());
        peliculaActualizar.put(ConfigBaseDatos.Peliculas.COLUMN_PAIS, pelicula.getPais());
        String[] args = new String[]{pelicula.getId()};

        // Realizamos el insert en la base de datos.
        db.update(ConfigBaseDatos.Peliculas.TABLE_NAME, peliculaActualizar, "id=?", args);
    }

    //Obtiene todas las calificacion
    public Cursor getAllPeliculas(SQLiteDatabase db){
        String[] fields = new String[] {
                ConfigBaseDatos.Peliculas.COLUMN_ID,
                ConfigBaseDatos.Peliculas.COLUMN_TITULO,
                ConfigBaseDatos.Peliculas.COLUMN_ACTORES,
                ConfigBaseDatos.Peliculas.COLUMN_URL_IMG,
                ConfigBaseDatos.Peliculas.COLUMN_FECHA_VISIONADO,
                ConfigBaseDatos.Peliculas.COLUMN_CIUDAD,
                ConfigBaseDatos.Peliculas.COLUMN_GENERO,
                ConfigBaseDatos.Peliculas.COLUMN_ANNIO,
                ConfigBaseDatos.Peliculas.COLUMN_PAIS
        };
        String[] args = new String[] {};
        return  db.query(ConfigBaseDatos.Peliculas.TABLE_NAME, fields, null, args, null, null, null);

    }

    //Obtiene una pelicula por su ID
    public Cursor getPeliculaById(SQLiteDatabase db,  int id) {
        String selectQuery = "SELECT  * FROM " + ConfigBaseDatos.Peliculas.TABLE_NAME + " Where " + ConfigBaseDatos.Peliculas.COLUMN_ID + " = " + id;
        return db.rawQuery(selectQuery, null);
    }

}
