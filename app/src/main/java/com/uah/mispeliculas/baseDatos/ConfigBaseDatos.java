package com.uah.mispeliculas.baseDatos;

import android.provider.BaseColumns;

public class ConfigBaseDatos {

    private String id;
    private String titulo;
    private String actores;
    private String sipnosis;
    private String urlImg;
    private String fechaVisionado;
    private String ciudad;


    /* Clase para definicar la tabla y sus columnas*/
    public static class Peliculas implements BaseColumns {
        public static final String TABLE_NAME = "peliculas";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TITULO = "titulo";
        public static final String COLUMN_ACTORES = "actores";
        public static final String COLUMN_URL_IMG = "urlImg";
        public static final String COLUMN_FECHA_VISIONADO = "fechaVisionado";
        public static final String COLUMN_CIUDAD = "ciudad";
        public static final String COLUMN_GENERO = "genero";
        public static final String COLUMN_ANNIO = "annio";
        public static final String COLUMN_PAIS = "pais";

    }

}
