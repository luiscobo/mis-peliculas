package com.uah.mispeliculas.model;

public class Pelicula {

    private String id;
    private String titulo;
    private String actores;
    private String sipnosis;
    private String urlImg;
    private String fechaVisionado;
    private String ciudad;
    private String genero;
    private String annio;
    private String pais;

    public Pelicula() {
    }

    public Pelicula(String id, String titulo, String actores, String urlImg, String fechaVisionado, String ciudad, String genero, String annio, String pais) {
        this.id = id;
        this.titulo = titulo;
        this.actores = actores;
        this.urlImg = urlImg;
        this.fechaVisionado = fechaVisionado;
        this.ciudad = ciudad;
        this.genero = genero;
        this.annio = annio;
        this.pais = pais;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getSipnosis() {
        return sipnosis;
    }

    public void setSipnosis(String sipnosis) {
        this.sipnosis = sipnosis;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getFechaVisionado() {
        return fechaVisionado;
    }

    public void setFechaVisionado(String fechaVisionado) {
        this.fechaVisionado = fechaVisionado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAnnio() {
        return annio;
    }

    public void setAnnio(String annio) {
        this.annio = annio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
