package com.uah.mispeliculas.model;

import java.io.Serializable;

public class PeliculaServer implements Serializable {

    private String Title;
    private String Year;
    private String Genre;
    private String Language;
    private String Country;
    private String Actors;
    private String Poster;
    private String Response;

    public PeliculaServer(String title, String year, String genre, String language, String country, String actors, String poster, String response) {
        Title = title;
        Year = year;
        Genre = genre;
        Language = language;
        Country = country;
        Actors = actors;
        Poster = poster;
        Response = response;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getActors() {
        return Actors;
    }

    public void setActors(String actors) {
        Actors = actors;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }
}
